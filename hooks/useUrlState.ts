import { TQueryParam } from '@/types';
import { useSearchParams, usePathname, useRouter } from 'next/navigation';

export default function useHandlerUrlStates() {
  const searchParams = useSearchParams();
  const params = new URLSearchParams(searchParams);
  const pathName = usePathname();
  const { replace } = useRouter();

  const handlerUpdateDeleteState = (status: TQueryParam) => {
    for (const key in status) {
      const value = status[key];

      if (value) params.set(key, value);
      else params.delete(key);
    }

    replace(`${pathName}?${params.toString()}`);
  };

  return {
    handlerUpdateDeleteState,
  };
}
