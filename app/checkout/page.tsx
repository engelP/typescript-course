import { createClient } from '@/utils/supabase/server';
import { GetAllCartDetails } from '@/actions/cartDetails/getAll';
import CartDetails from '@/components/Cart/CartDetails';
import Layout from '@/components/layout';
import { cartProductType } from '@/types';
import { calcSubTotal, calcTotal } from '@/utils/calcTotal';
import { redirect } from 'next/navigation';
import { getCartUser } from '@/actions/carts/getById';
import CheckoutForm from '@/components/CheckoutForm';

export default async function CheckoutPage() {
  const supabase = createClient();

  const {
    data: { user },
  } = await supabase.auth.getUser();

  if (!user) {
    return redirect('/login');
  }

  const cartDetails: cartProductType[] = await GetAllCartDetails();
  const total: number = calcTotal(calcSubTotal(cartDetails));
  const cart_id: number = await getCartUser();

  return (
    <Layout titlePage="Checkout Page">
      <div className="font-[sans-serif] bg-white p-4 min-h-full">
        <div className="lg:max-w-6xl max-w-xl mx-auto">
          <div className="grid lg:grid-cols-3 gap-8">
            <div className="lg:col-span-2 max-lg:order-1">
              <h2 className="text-3xl font-extrabold text-[#333]">
                Make a payment
              </h2>
              <p className="text-[#333] text-base mt-6">
                Complete your transaction swiftly and securely with our
                easy-to-use payment process.
              </p>
              <CheckoutForm cart_id={cart_id} total={total} />
            </div>
            <div className="bg-gray-100 p-6 rounded-md">
              <h2 className="text-4xl font-extrabold text-[#333]">${total}</h2>
              <CartDetails dataCartDetails={cartDetails} />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
