import { createClient } from '@/utils/supabase/server';
import Layout from '@/components/layout';
import { redirect } from 'next/navigation';
import Table from '@/components/Table';
import { Order } from '@/models/order.model';
import { getAllOrders } from '@/actions/orders/getAll';

export default async function OrderPage() {
  const supabase = createClient();

  const {
    data: { user },
  } = await supabase.auth.getUser();

  if (!user) {
    return redirect('/login');
  }

  const headsTable: string[] = [
    'ID',
    'Shipping address',
    'Payment method',
    'Total',
    'Order status',
  ];

  const dataOrders: Order[] = await getAllOrders();

  return (
    <Layout titlePage="Orders Page">
      <Table heads={headsTable} tableBody={dataOrders} />
    </Layout>
  );
}
