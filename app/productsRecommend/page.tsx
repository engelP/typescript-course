import { createClient } from '@/utils/supabase/server';
import { redirect } from 'next/navigation';
import Cards from '@/components/Cards';
import { Product } from '@/models/product.model';
import { GetAllProduct } from '@/actions/products/getAll';
import SearchInput from '@/components/SearchInput';
import { TSearchParams } from '@/types';
import Layout from '@/components/layout';

export default async function ProductsRecommendPage({
  searchParams,
}: TSearchParams) {
  const supabase = createClient();

  const {
    data: { user },
  } = await supabase.auth.getUser();

  if (!user) {
    return redirect('/login');
  }

  const search = searchParams.search || '';
  const action = searchParams.action || '';
  const products: Product[] = await GetAllProduct(search);

  return (
    <Layout isVisibleCart titlePage="Products Recommend" action={action}>
      <SearchInput />
      <div className="animate-in flex-1 flex flex-col gap-20 opacity-0 max-w-7xl px-2">
        <main className="w-full p-2">
          <Cards data={products} />
        </main>
      </div>
    </Layout>
  );
}
