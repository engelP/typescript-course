import { NextRequest, NextResponse } from 'next/server';
import { createClient } from '@/utils/supabase/server';

// update order
export const PATCH = async (
  req: NextRequest,
  context: any
): Promise<NextResponse> => {
  const supabase = createClient();
  const { id } = context.params;

  const dataOrder = await req.json();

  if (isNaN(id))
    return NextResponse.json({
      status: 400,
      body: { message: 'Invalid order id :(' },
    });

  const { data: product, error } = await supabase
    .from('orders')
    .update({ ...dataOrder })
    .eq('id', id)
    .select()
    .single();

  if (error)
    return NextResponse.json({
      status: 500,
      error: { message: error.message },
    });

  return NextResponse.json({
    status: 200,
    body: { data: product },
  });
};
