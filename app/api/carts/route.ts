import { NextRequest, NextResponse } from 'next/server';
import { createClient } from '@/utils/supabase/server';

export const GET = async (req: NextRequest): Promise<NextResponse> => {
  const supabase = createClient();
  const user_id = (await req.headers.get('user-id')) || '';

  const { data: cart, error } = await supabase
    .from('carts')
    .select()
    .eq('user_id', user_id)
    .is('deleted_at', null)
    .single();

  if (!cart?.id)
    return NextResponse.json({
      status: 500,
      error: { message: 'Cart not found...' },
    });

  return NextResponse.json({
    status: 200,
    body: { data: cart?.id },
  });
};
