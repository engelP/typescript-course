import { createClient } from '@/utils/supabase/server';
import { NextRequest, NextResponse } from 'next/server';

// get all product
export const GET = async (req: NextRequest): Promise<NextResponse> => {
  const supabase = createClient();
  const search = req.nextUrl.searchParams.get('search') || '';

  const { data: products, error } = await supabase
    .from('products')
    .select()
    .is('deleted_at', null)
    .like('name', `%${search}%`);

  if (error)
    return NextResponse.json({
      status: 500,
      error: { message: error.message },
    });

  return NextResponse.json({
    body: { data: products },
    status: 200,
  });
};

// create product
export const POST = async (req: NextRequest): Promise<NextResponse> => {
  const supabase = createClient();
  const dataProduct = await req.json();

  const { data: products, error } = await supabase
    .from('products')
    .insert([{ ...dataProduct }])
    .select();

  if (error)
    return NextResponse.json({
      status: 500,
      error: { message: error.message },
    });

  return NextResponse.json({
    status: 200,
    body: { data: products },
  });
};
