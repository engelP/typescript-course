import { createClient } from '@/utils/supabase/server';
import { NextRequest, NextResponse } from 'next/server';

// get all cart details
export const GET = async (req: NextRequest): Promise<NextResponse> => {
  const supabase = createClient();
  const user_id = (await req.headers.get('user-id')) || '';

  const cart_id: number = await getCart(user_id);

  const { data: cartDetails, error } = await supabase
    .from('cartDetails')
    .select('id, cart_id, quantity, products (id, name, image, price)')
    .eq('cart_id', cart_id)
    .is('deleted_at', null);

  if (error)
    return NextResponse.json({
      status: 500,
      error: { message: error.message },
    });

  return NextResponse.json({
    body: { data: cartDetails },
    status: 200,
  });
};

// create cart details
export const POST = async (req: NextRequest): Promise<NextResponse> => {
  const supabase = createClient();
  const dataCartDetails = await req.json();
  const product_id = dataCartDetails.product_id || 0;

  const user_id = (await req.headers.get('user-id')) || '';
  const cart_id: number = await getCart(user_id);

  const existProduct: number = await getProduct(product_id, cart_id);

  if (existProduct)
    return NextResponse.json({
      status: 500,
      error: { message: 'Product already exists' },
    });

  const { data: cartDetails, error } = await supabase
    .from('cartDetails')
    .insert([{ ...dataCartDetails, cart_id }])
    .select()
    .single();

  if (error)
    return NextResponse.json({
      status: 500,
      error: { message: error.message },
    });

  return NextResponse.json({
    body: { data: cartDetails },
    status: 200,
  });
};

const getCart = async (user_id: string): Promise<number> => {
  const supabase = createClient();

  const { data: cart, error } = await supabase
    .from('carts')
    .select()
    .eq('user_id', user_id)
    .is('deleted_at', null)
    .single();

  if (error) return 0;

  if (!cart) {
    const { data: newCart } = await supabase
      .from('carts')
      .insert([{ user_id }])
      .select()
      .single();

    return newCart?.id;
  }

  return cart?.id;
};

const getProduct = async (
  product_id: number,
  cart_id: number
): Promise<number> => {
  const supabase = createClient();

  const { data: products, error } = await supabase
    .from('cartDetails')
    .select()
    .is('deleted_at', null)
    .eq('product_id', product_id)
    .eq('cart_id', cart_id)
    .single();

  if (error) return 0;

  return products?.id || 0;
};
