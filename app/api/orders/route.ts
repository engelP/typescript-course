import { createClient } from '@/utils/supabase/server';
import { NextRequest, NextResponse } from 'next/server';

export const GET = async (): Promise<NextResponse> => {
  const supabase = createClient();

  const { data: orders, error } = await supabase
    .from('orders')
    .select()
    .is('deleted_at', null)
    .order('id', { ascending: true });

  if (error)
    return NextResponse.json({
      status: 500,
      error: { message: error.message },
    });

  return NextResponse.json({
    body: { data: orders },
    status: 200,
  });
};

// create order
export const POST = async (req: NextRequest): Promise<NextResponse> => {
  const supabase = createClient();
  const dataOrders = await req.json();
  const user_id = (await req.headers.get('user-id')) || '';

  if (!user_id)
    return NextResponse.json({
      status: 500,
      error: { message: 'Could not obtain user id :(' },
    });

  const { data: order, error } = await supabase
    .from('orders')
    .insert([{ ...dataOrders, user_id }])
    .select()
    .single();

  if (error)
    return NextResponse.json({
      status: 500,
      error: { message: error.message },
    });

  await cleanCart(order?.cart_id || 0);

  return NextResponse.json({
    body: { data: order },
    status: 200,
  });
};

const cleanCart = async (cart_id: number): Promise<boolean> => {
  const supabase = createClient();

  const { error } = await supabase
    .from('cartDetails')
    .update({ deleted_at: new Date().toISOString() })
    .eq('cart_id', cart_id);

  if (error) return false;

  return true;
};
