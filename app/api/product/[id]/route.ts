import { NextRequest, NextResponse } from 'next/server';
import { createClient } from '@/utils/supabase/server';

// get one product
export const GET = async (
  _req: NextRequest,
  context: any
): Promise<NextResponse> => {
  const supabase = createClient();
  const { id } = context.params;

  if (isNaN(id))
    return NextResponse.json({
      status: 400,
      body: { message: 'Invalid product id :(' },
    });

  const exist: boolean = await existProduct(id);

  if (!exist)
    return NextResponse.json({
      status: 400,
      body: { message: 'Product not found :(' },
    });

  const { data: products, error } = await supabase
    .from('products')
    .select()
    .is('deleted_at', null)
    .single();

  if (error)
    return NextResponse.json({
      status: 500,
      error: { message: error.message },
    });

  return NextResponse.json({
    status: 200,
    body: { data: products },
  });
};

// update product
export const PATCH = async (
  req: NextRequest,
  context: any
): Promise<NextResponse> => {
  const supabase = createClient();
  const { id } = context.params;

  const dataProduct = await req.json();

  if (isNaN(id))
    return NextResponse.json({
      status: 400,
      body: { message: 'Invalid product id :(' },
    });

  const { data: product, error } = await supabase
    .from('products')
    .update({ ...dataProduct })
    .eq('id', id)
    .select()
    .single();

  if (error)
    return NextResponse.json({
      status: 500,
      error: { message: error.message },
    });

  return NextResponse.json({
    status: 200,
    body: { data: product },
  });
};

// custom function
const existProduct = async (id: number): Promise<boolean> => {
  const supabase = createClient();

  const { data: product } = await supabase
    .from('products')
    .select()
    .eq('id', id)
    .is('deleted_at', null)
    .single();

  if (!product) return false;

  return true;
};
