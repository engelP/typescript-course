import { NextRequest, NextResponse } from 'next/server';
import { createClient } from '@/utils/supabase/server';

// delete cart details
export const DELETE = async (
  req: NextRequest,
  context: any
): Promise<NextResponse> => {
  const supabase = createClient();
  const { id } = context.params;

  if (isNaN(id))
    return NextResponse.json({
      status: 400,
      body: { message: 'Invalid cart details id :(' },
    });

  const { error } = await supabase
    .from('cartDetails')
    .update({ deleted_at: new Date().toISOString() })
    .eq('product_id', id);

  console.log(error, id);

  if (error)
    return NextResponse.json({
      status: 500,
      error: { message: error.message },
    });

  return NextResponse.json({
    status: 200,
    body: { message: 'Successfully deleted' },
  });
};
