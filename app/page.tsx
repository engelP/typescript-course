import { Product } from '@/models/product.model';
import { GetAllProduct } from '@/actions/products/getAll';
import Cards from '@/components/Cards';
import SearchInput from '@/components/SearchInput';
import { TSearchParams } from '@/types';
import Layout from '@/components/layout';

export default async function Index({ searchParams }: TSearchParams) {
  const search = searchParams.search || '';
  const action = searchParams.action || '';
  const products: Product[] = await GetAllProduct(search);

  return (
    <Layout isVisibleCart titlePage="Home page Shopping App" action={action}>
      <SearchInput />
      <div className="animate-in flex-1 flex flex-col gap-20 opacity-0 max-w-7xl px-2">
        <main className="w-full p-2">
          <Cards data={products} />
        </main>
      </div>
    </Layout>
  );
}
