import axios from 'axios';
import { createClient } from './supabase/server';

export const axiosInstance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL_BASE!,
});

axiosInstance.interceptors.request.use(
  async (config) => {
    const client = createClient();
    const { data } = await client.auth.getUser();

    config.headers['user-id'] = data.user?.id || '';

    return config;
  },
  (error) => Promise.reject(error)
);
