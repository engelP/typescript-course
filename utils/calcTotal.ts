import { cartProductType } from '@/types';

export const calcSubTotal = (dataCartDetails: cartProductType[]): number => {
  let subtotal: number = 0;

  dataCartDetails.forEach(
    ({ products }) => (subtotal += products.price as number)
  );

  return Math.round(subtotal * 100) / 100;
};

export const calcTotal = (subtotal: number): number => {
  let total: number = 0;
  const IVA: number = subtotal * 0.15;
  total = IVA + subtotal;

  return Math.round(total * 100) / 100;
};
