export class Cart {
  id?: number;
  user_id: string;
  crated_at?: Date;
  deleted_at?: Date | null;

  constructor(cart: Cart) {
    this.user_id = cart.user_id;
    this.deleted_at = cart.deleted_at || null;
  }
}
