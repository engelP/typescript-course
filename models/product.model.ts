import { noneType } from '@/types';

export class Product {
  id?: number;
  name: string;
  description: string;
  price: number | noneType;
  available: boolean;
  image: string;
  created_at?: Date;
  deleted_at?: Date | null;

  constructor(product: Product) {
    this.name = product.name || '';
    this.description = product.description || '';
    this.price = product.price || 0;
    this.available = product.available || true;
    this.image = product.image;
    this.deleted_at = product.deleted_at || null;
  }
}
