import { orderStatusType, paymentMethodType } from '@/types';

export class Order {
  id?: number;
  user_id: string;
  cart_id: number;
  total: number;
  shopping_address: string;
  payment_method: paymentMethodType;
  order_status: orderStatusType;
  created_at?: Date;
  deleted_at?: Date | null;

  constructor(order: Order) {
    this.user_id = order.user_id;
    this.cart_id = order.cart_id;
    this.total = order.total;
    this.shopping_address = order.shopping_address;
    this.payment_method = order.payment_method || 'None';
    this.order_status = order.order_status || 'None';
    this.deleted_at = order.deleted_at || null;
  }
}
