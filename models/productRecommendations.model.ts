export class ProductRecommendations {
  id?: number | undefined;
  user_id: string;
  product_id: number;
  deleted_at?: Date | null;
  purchase_date?: Date | null;

  constructor(purchase: ProductRecommendations) {
    this.user_id = purchase.user_id;
    this.product_id = purchase.product_id;
  }
}
