export class CartDetails {
  id?: number;
  cart_id: number;
  product_id: number;
  quantity: number;
  deleted_at?: Date | null;

  constructor(cartDetails: CartDetails) {
    this.cart_id = cartDetails.cart_id;
    this.product_id = cartDetails.product_id;
    this.quantity = cartDetails.quantity || 0;
    this.deleted_at = cartDetails.deleted_at || null;
  }
}
