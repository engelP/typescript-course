export class User {
  id?: number;
  name: string;
  address: string;
  created_at?: Date;
  email: string;
  password: string;
  deleted_at?: Date | null;

  constructor(user: User) {
    this.name = user.name || '';
    this.address = user.address || '';
    this.email = user.email || '';
    this.password = user.password || '';
    this.deleted_at = user.deleted_at || null;
  }
}
