export class PurchaseHistory {
  id?: number | undefined;
  product_id: number;
  user_id: string;
  deleted_at?: Date | null;
  recommendation_date?: Date | null;

  constructor(purchase: PurchaseHistory) {
    this.product_id = purchase.product_id;
    this.user_id = purchase.user_id;
  }
}
