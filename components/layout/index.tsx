import Footer from '../Footer';
import Header from '../Header';
import SearchInput from '../SearchInput';

interface IPropsLayout {
  children: React.ReactNode;
  action?: string;
  titlePage: string;
  isVisibleCart?: boolean;
}

export default function Layout({
  children,
  action = '',
  titlePage,
  isVisibleCart = false,
}: IPropsLayout) {
  return (
    <div className="flex-1 w-full flex flex-col gap-10 items-center">
      <div className="w-full p-4">
        <Header
          isVisibleCart={isVisibleCart}
          isOpenCart={!action}
          title="ShoppingApp"
        />
      </div>
      <h2 className="self-center text-4xl font-semibold whitespace-nowrap dark:text-white">
        {titlePage}
      </h2>
      {children}
      <Footer />
    </div>
  );
}
