'use client';

import { createOrder } from '@/actions/orders/create';
import Link from 'next/link';
import { useFormState } from 'react-dom';
import { FaCreditCard } from 'react-icons/fa';

interface IPropsCheckoutForm {
  total: number;
  cart_id: number;
}

export default function CheckoutForm({ cart_id, total }: IPropsCheckoutForm) {
  const [_stateCreate, formActionCreate] = useFormState(createOrder, '');

  return (
    <form action={formActionCreate} className="mt-12 max-w-lg">
      <div className="grid gap-6">
        <input type="hidden" name="cart_id" value={cart_id} />
        <input
          type="text"
          name="shopping_address"
          placeholder="shopping address"
          className="px-4 py-3.5 bg-gray-100 text-[#333] w-full text-sm border rounded-md focus:border-purple-500 outline-none"
          required
        />
        <input
          type="text"
          name="total"
          value={total}
          placeholder="Total"
          className="px-4 py-3.5 bg-gray-100 text-[#333] w-full text-sm border rounded-md focus:border-purple-500 outline-none"
          required
          readOnly
        />
        <div className="flex bg-gray-100 border items-center px-2 rounded-md focus-within:border-purple-500 overflow-hidden">
          <FaCreditCard className="text-gray-600" size={30} />
          <input
            type="number"
            name="card_number"
            placeholder="Card Number"
            className="px-4 py-3.5 bg-gray-100 text-[#333] w-full text-sm outline-none"
            required
          />
        </div>
        <div className="grid grid-cols-2 gap-6">
          <input
            type="date"
            name="date"
            placeholder="EXP."
            className="px-4 py-3.5 bg-gray-100 text-[#333] w-full text-sm border rounded-md focus:border-purple-500 outline-none"
            required
          />
          <input
            type="number"
            name="cvv"
            placeholder="CVV"
            className="px-4 py-3.5 bg-gray-100 text-[#333] w-full text-sm border rounded-md focus:border-purple-500 outline-none"
            required
          />
        </div>
      </div>
      <div className="flex gap-5">
        <button
          disabled={total ? false : true}
          type="submit"
          className="mt-6 w-40 py-3.5 text-sm bg-purple-500 text-white rounded-md hover:bg-purple-600"
        >
          Submit
        </button>
        <Link
          className=" text-center mt-6 w-40 py-3.5 text-sm bg-purple-500 text-white rounded-md hover:bg-purple-600"
          href={'/products'}
        >
          Cancel
        </Link>
      </div>
    </form>
  );
}
