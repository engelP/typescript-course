import { FaCartShopping } from 'react-icons/fa6';
import { FaGripVertical } from 'react-icons/fa6';
import Link from 'next/link';
import AuthButton from './AuthButton';
import Cart from './Cart';
import { GetAllCartDetails } from '@/actions/cartDetails/getAll';
import { Product } from '@/models/product.model';
import { cartProductType } from '@/types';
import { getSession } from '@/utils/getSession';

interface IPropsHeader {
  title: string;
  isOpenCart: boolean;
  isVisibleCart: boolean;
}

export default async function Header({
  title,
  isOpenCart,
  isVisibleCart = false,
}: IPropsHeader) {
  const cartDetails: cartProductType[] = await GetAllCartDetails();
  const user = await getSession();

  return (
    <nav className="bg-white dark:bg-gray-900 fixed w-full z-20 top-0 start-0 border-b border-gray-200 dark:border-gray-600 sticky top-0">
      <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <Link
          href={'/'}
          className="flex items-center space-x-3 rtl:space-x-reverse"
        >
          <FaCartShopping size={30} />
          <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">
            {title}
          </span>
        </Link>
        <div className="flex md:order-2 space-x-3 md:space-x-0 rtl:space-x-reverse gap-3">
          <AuthButton />
          <button
            data-collapse-toggle="navbar-sticky"
            type="button"
            className="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
            aria-controls="navbar-sticky"
            aria-expanded="false"
          >
            <span className="sr-only">Open main menu</span>
            <FaGripVertical size={20} />
          </button>
          {user && isVisibleCart && (
            <Cart isOpen={isOpenCart} dataCartDetails={cartDetails} />
          )}
        </div>
        <div
          className="items-center justify-between hidden w-full md:flex md:w-auto md:order-1"
          id="navbar-sticky"
        >
          <ul className="flex flex-col p-4 md:p-0 mt-4 font-medium border border-gray-100 rounded-lg bg-gray-50 md:space-x-8 rtl:space-x-reverse md:flex-row md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
            <li>
              <Link
                href={'/'}
                className="block py-2 px-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 md:dark:text-blue-500"
              >
                Home
              </Link>
            </li>
            <li>
              <Link
                href={'/products'}
                className="block py-2 px-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 md:dark:text-blue-500"
              >
                Products
              </Link>
            </li>
            <li>
              <Link
                href={'/productsRecommend'}
                className="block py-2 px-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 md:dark:text-blue-500"
              >
                Products Recommend
              </Link>
            </li>
            <li>
              <Link
                href={'/orders'}
                className="block py-2 px-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 md:dark:text-blue-500"
              >
                Orders
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
