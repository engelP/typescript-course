import { Product } from '@/models/product.model';
import { getSession } from '@/utils/getSession';
import Card from '../Card';

interface IPropsCards {
  data: Product[];
}

export default function Cards({ data }: IPropsCards) {
  const renderCard = async () => {
    const isButton: boolean = await getSession();

    return data.map((product) => (
      <Card
        key={product.id}
        id={product.id}
        name={product.name}
        image={product.image}
        price={product.price}
        available={product.available ? 'Available' : 'Not available'}
        description={product.description}
        isButton={isButton && product.available}
      />
    ));
  };

  return (
    <div className="w-full max-h-96 h-full flex flex-wrap gap-5 p-4 overflow-y-scroll">
      {renderCard()}
    </div>
  );
}
