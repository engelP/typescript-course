'use client';

import useHandlerUrlStates from '@/hooks/useUrlState';
import { ECartActions, cartProductType } from '@/types';
import { FaCartArrowDown } from 'react-icons/fa';
import { FaSquareXmark } from 'react-icons/fa6';
import CartDetails from './CartDetails';
import CartStats from './CartStats';

interface IPropsCartButton {
  dataCartDetails: cartProductType[];
  isOpen: boolean;
}

export default function CartButton({
  dataCartDetails,
  isOpen,
}: IPropsCartButton) {
  const { handlerUpdateDeleteState } = useHandlerUrlStates();

  const onOpenCart = (): void =>
    handlerUpdateDeleteState({
      action: ECartActions.OPEN,
    });

  const onCloseCart = (): void =>
    handlerUpdateDeleteState({
      action: '',
    });

  return (
    <>
      <button
        onClick={onOpenCart}
        type="button"
        className="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
      >
        <FaCartArrowDown size={30} />
      </button>

      <div
        className={`absolute top-full right-0 z-40 h-96 overflow-y-auto transition-all duration-500 bg-white dark:bg-gray-800 w-96 p-4 ${
          isOpen
            ? 'translate-y-16 invisible opacity-0'
            : 'translate-y-1 visible opacity-100'
        }`}
        aria-labelledby="drawer-navigation-label"
      >
        <h5
          id="drawer-navigation-label"
          className="text-base font-semibold text-gray-500 uppercase dark:text-gray-400"
        >
          Shopping Cart
        </h5>
        <button
          type="button"
          onClick={onCloseCart}
          className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 absolute top-2.5 end-2.5 inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
        >
          <FaSquareXmark size={30} />
        </button>
        <div className="py-4 overflow-y-auto">
          <CartDetails hiddenButton dataCartDetails={dataCartDetails} />
          <CartStats
            dataCartDetails={dataCartDetails}
            onCloseCart={onCloseCart}
          />
        </div>
      </div>
    </>
  );
}
