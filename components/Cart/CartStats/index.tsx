import { cartProductType } from '@/types';
import { calcSubTotal, calcTotal } from '@/utils/calcTotal';
import Link from 'next/link';

interface IPropsCartStats {
  dataCartDetails: cartProductType[];
  onCloseCart(): void;
}

export default function CartStats({
  dataCartDetails,
  onCloseCart,
}: IPropsCartStats) {
  return (
    <div className="border-t border-gray-200 px-4 py-6 sm:px-6">
      <div className="flex justify-between text-base font-medium text-gray-300">
        <p>Subtotal</p>
        <p>${calcSubTotal(dataCartDetails)}</p>
      </div>
      <div className="flex justify-between text-base font-medium text-gray-300">
        <p>Total</p>
        <p>${calcTotal(calcSubTotal(dataCartDetails))}</p>
      </div>
      <p className="mt-0.5 text-sm text-gray-500">Shopping App Tax is 15%.</p>
      <div className="mt-6">
        <Link
          href="/checkout"
          className={`${
            dataCartDetails.length
              ? 'bg-indigo-600 hover:bg-indigo-700'
              : 'pointer-events-none bg-indigo-400'
          } flex items-center justify-center rounded-md border border-transparent  px-6 py-3 text-base font-medium text-white shadow-sm `}
        >
          Checkout
        </Link>
      </div>
      <div className="mt-6 flex justify-center text-center text-sm text-gray-500">
        <p>
          or{' '}
          <button
            type="button"
            onClick={onCloseCart}
            className="font-medium text-indigo-600 hover:text-indigo-500"
          >
            Continue Shopping
            <span aria-hidden="true"> &rarr;</span>
          </button>
        </p>
      </div>
    </div>
  );
}
