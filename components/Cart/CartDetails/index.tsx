import { deleteCartDetails } from '@/actions/cartDetails/delete';
import { cartProductType } from '@/types';
import Image from 'next/image';

interface IPropsCartDetails {
  dataCartDetails: cartProductType[];
  hiddenButton?: boolean;
}

export default function CartDetails({
  dataCartDetails,
  hiddenButton = false,
}: IPropsCartDetails) {
  const handlerDelete = async (id: number): Promise<void> => {
    if (id) await deleteCartDetails(id);
  };

  const renderCartDetails = () =>
    dataCartDetails.map((cartD) => {
      return (
        <li key={`products-${cartD.products.id}`} className="flex py-6">
          <div className="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
            <Image
              src={cartD.products.image}
              alt="cart product"
              className="h-full w-full object-cover object-center"
              width={100}
              height={100}
            />
          </div>

          <div className="ml-4 flex flex-1 flex-col">
            <div>
              <div className="text-base font-medium text-gray-400">
                <h3>{cartD.products.name}</h3>
                <p>Price: ${cartD.products.price}</p>
              </div>
            </div>
            <div className="flex flex-1 items-end justify-between text-sm">
              <p className="text-gray-500">Quantity: {cartD.quantity}</p>
              {hiddenButton && (
                <div className="flex">
                  <button
                    onClick={() => handlerDelete(cartD.products.id || 0)}
                    type="button"
                    className="font-medium text-red-400 hover:text-gray-500"
                  >
                    Remove
                  </button>
                </div>
              )}
            </div>
          </div>
        </li>
      );
    });

  return <ul className="divide-y divide-gray-200">{renderCartDetails()}</ul>;
}
