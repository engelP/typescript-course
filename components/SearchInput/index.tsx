'use client';
import useHandlerUrlStates from '@/hooks/useUrlState';
import { ChangeEvent, FormEvent } from 'react';
import { FaSearch } from 'react-icons/fa';

export default function SearchInput() {
  const { handlerUpdateDeleteState } = useHandlerUrlStates();

  const handlerSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const { value } = e.currentTarget['search'];

    handlerUpdateDeleteState({
      search: value || '',
    });
  };

  return (
    <form onSubmit={handlerSubmit} className="max-w-lg w-full mx-auto">
      <label
        htmlFor="default-search"
        className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
      >
        Search
      </label>
      <div className="relative">
        <div className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
          <FaSearch />
        </div>
        <input
          type="search"
          id="default-search"
          className="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          placeholder={'Search products...'}
          name="search"
        />
        <button
          type="submit"
          className="text-white absolute end-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Search
        </button>
      </div>
    </form>
  );
}
