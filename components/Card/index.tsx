'use client';
import { createCartDetails } from '@/actions/cartDetails/create';
import { CartDetailsType, availableType, noneType } from '@/types';
import Image from 'next/image';

interface IPropsCard {
  id?: number;
  name: string;
  description: string;
  price: number | noneType;
  available: availableType;
  image: string;
  isButton?: boolean;
}

export default function Card({
  id,
  image,
  description,
  price,
  available,
  name,
  isButton = false,
}: IPropsCard) {
  const handlerClick = async (): Promise<void> => {
    if (id) {
      const cartsDetails: CartDetailsType = {
        quantity: 1,
        product_id: id,
      };

      await createCartDetails(cartsDetails);
    }
  };

  return (
    <div className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <Image
        className="p-8 rounded-t-lg"
        src={image}
        alt="product image"
        width={300}
        height={300}
      />
      <div className="px-5 pb-5">
        <div className="flex justify-between">
          <h5 className="text-xl font-semibold tracking-tight text-gray-900 dark:text-white">
            {name}
          </h5>

          <span className="mb-3 font-normal text-gray-700 dark:text-gray-400 inline-block">
            {available}
          </span>
        </div>
        <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
          {description}
        </p>
        <div className="flex items-center justify-between">
          <span className="text-3xl font-bold text-gray-900 dark:text-white">
            ${price}
          </span>

          {isButton && (
            <button
              onClick={handlerClick}
              type="button"
              className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Add to cart
            </button>
          )}
        </div>
      </div>
    </div>
  );
}
