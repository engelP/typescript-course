interface IPropsTableHead {
  heads: string[];
}

export default function TableHead({ heads }: IPropsTableHead) {
  const renderHead = () =>
    heads.map((head) => (
      <th key={head} scope="col" className="px-6 py-3">
        {head}
      </th>
    ));

  return (
    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
      <tr>{renderHead()}</tr>
    </thead>
  );
}
