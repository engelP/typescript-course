import { Order } from '@/models/order.model';
import TableHead from './TableHead';
import TableBody from './TableBody';

interface IPropsTable {
  heads: string[];
  tableBody: Order[];
}

export default function Table({ heads, tableBody }: IPropsTable) {
  return (
    <div className="relative overflow-x-auto">
      <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
        <TableHead heads={heads} />
        <TableBody tableBody={tableBody} />
      </table>
    </div>
  );
}
