'use client';

import { updateOrder } from '@/actions/orders/update';
import { Order } from '@/models/order.model';
import { EOrderStatus } from '@/types';
import { useFormState } from 'react-dom';
import { FaSave } from 'react-icons/fa';

interface IPropsTableBody {
  tableBody: Order[];
}

export default function TableBody({ tableBody }: IPropsTableBody) {
  const [_stateUpdate, formActionUpdate] = useFormState(updateOrder, '');

  const status: string[] = [
    EOrderStatus.DELIVERED,
    EOrderStatus.PENDING,
    EOrderStatus.PROCESSED,
    EOrderStatus.SHIPPED,
  ];

  const renderOption = (statusSelected: string) =>
    status.map((stat) => (
      <option key={stat} value={stat} selected={stat === statusSelected}>
        {stat}
      </option>
    ));

  const renderBody = () =>
    tableBody.map((item) => (
      <tr
        key={item.id}
        className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
      >
        <td
          scope="row"
          className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
        >
          {item.id}
        </td>
        <td className="px-6 py-4">{item.shopping_address}</td>
        <td className="px-6 py-4">{item.payment_method}</td>
        <td className="px-6 py-4">${item.total}</td>
        <td className="px-6 py-4">
          <form action={formActionUpdate} className="max-w-sm mx-auto">
            <input type="hidden" name="order_id" value={item.id} />
            <div className="flex gap-2">
              <select
                name="status"
                defaultValue={item.order_status}
                onChange={() => {}}
                id="status"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              >
                {renderOption(item.order_status)}
              </select>
              <button
                type="submit"
                className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 end-2.5 inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
              >
                <FaSave size={25} />
              </button>
            </div>
          </form>
        </td>
      </tr>
    ));

  return <tbody>{renderBody()}</tbody>;
}
