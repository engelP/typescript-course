import { Product } from '@/models/product.model';

export type noneType = 'None';

export type paymentMethodType = 'PayPal' | 'Card' | 'Cash' | noneType;

export type orderStatusType =
  | 'pending'
  | 'processed'
  | 'shipped'
  | 'delivered'
  | noneType;

export type axiosResponse<T> = {
  body: {
    data: T;
  };
  status: number;
};

export type availableType = 'Available' | 'Not available';

export type TQueryParam = {
  [key in string]: string;
};

export type TSearchParams = {
  searchParams: {
    [key: string]: string;
  };
};

export type actionResult = 'success' | 'failed';

export type CartDetailsType = {
  quantity: number;
  product_id: number;
};

export enum ECartActions {
  OPEN = 'OPEN',
}

export type cartProductType = {
  id: number;
  cart_id: number;
  quantity: number;
  products: Product;
};

export enum EOrderStatus {
  PENDING = 'pending',
  PROCESSED = 'processed',
  SHIPPED = 'shipped',
  DELIVERED = 'delivered',
}
