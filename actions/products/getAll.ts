'use server';

import { Product } from '@/models/product.model';
import { axiosResponse } from '@/types/';
import { axiosInstance } from '@/utils/axios';
import { revalidatePath } from 'next/cache';

export async function GetAllProduct(search?: string): Promise<Product[]> {
  try {
    const {
      data: { body },
    } = await axiosInstance.get<axiosResponse<Product[]>>(
      `/products?search=${search}`
    );

    revalidatePath('/products');
    return body.data;
  } catch (error) {
    console.error(error);
  }

  return [];
}
