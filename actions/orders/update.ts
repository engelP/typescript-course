'use server';

import { axiosInstance } from '@/utils/axios';
import { revalidatePath } from 'next/cache';
import { ActionResult } from 'next/dist/server/app-render/types';

export async function updateOrder(
  _prevState: string,
  formData: FormData
): Promise<ActionResult> {
  try {
    const id = formData.get('order_id') as string;
    const order_status = formData.get('status') as string;

    if (id && order_status) {
      await axiosInstance.patch(`/order/${id}`, {
        order_status,
      });
    }

    revalidatePath('/order');

    return 'success';
  } catch (error) {
    return 'failed';
  }
}
