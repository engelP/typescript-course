'use server';

import { Order } from '@/models/order.model';
import { axiosResponse } from '@/types/';
import { axiosInstance } from '@/utils/axios';
import { revalidatePath } from 'next/cache';

export async function getAllOrders(): Promise<Order[]> {
  try {
    const {
      data: { body },
    } = await axiosInstance.get<axiosResponse<Order[]>>(`/orders`);

    revalidatePath('/orders');
    return body.data;
  } catch (error) {
    console.error(error);
  }

  return [];
}
