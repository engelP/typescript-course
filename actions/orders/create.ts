'use server';

import { orderStatusType, paymentMethodType } from '@/types';
import { axiosInstance } from '@/utils/axios';
import { revalidatePath } from 'next/cache';
import { redirect } from 'next/navigation';
import { ActionResult } from 'next/dist/server/app-render/types';

export async function createOrder(
  _prevState: string,
  formData: FormData
): Promise<ActionResult> {
  try {
    const cart_id = formData.get('cart_id') as string;
    const shopping_address = formData.get('shopping_address') as string;
    const total = formData.get('total') as string;
    const payment_method: paymentMethodType = 'Card';
    const order_status: orderStatusType = 'pending';

    if (
      cart_id &&
      shopping_address &&
      payment_method &&
      order_status &&
      total
    ) {
      await axiosInstance.post(`/orders`, {
        cart_id,
        shopping_address,
        payment_method,
        order_status,
        total,
      });
    }

    revalidatePath('/orders');

    return 'success';
  } catch (error) {
    return 'failed';
  }
}
