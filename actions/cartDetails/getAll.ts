'use server';

import { axiosResponse, cartProductType } from '@/types/';
import { axiosInstance } from '@/utils/axios';
import { revalidatePath } from 'next/cache';

export async function GetAllCartDetails(
  search?: string
): Promise<cartProductType[]> {
  try {
    const {
      data: { body },
    } = await axiosInstance.get<axiosResponse<cartProductType[]>>(
      `/cartDetails?search=${search}`
    );

    revalidatePath('/cartDetails');
    return body.data;
  } catch (error) {
    console.error(error);
  }

  return [];
}
