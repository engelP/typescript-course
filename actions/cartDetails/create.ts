'use server';

import { CartDetails } from '@/models/cartDetails.model';
import { CartDetailsType, actionResult, axiosResponse } from '@/types';
import { axiosInstance } from '@/utils/axios';
import { revalidatePath } from 'next/cache';

export async function createCartDetails(
  cartDetails: CartDetailsType
): Promise<actionResult> {
  try {
    const product_id = cartDetails.product_id;
    const quantity = cartDetails.quantity;

    if (product_id && quantity) {
      await axiosInstance.post<axiosResponse<CartDetails>>(`/cartDetails`, {
        product_id,
        quantity,
      });
    }

    revalidatePath('/cartDetails');
    return 'success';
  } catch (error) {
    return 'failed';
  }
}
