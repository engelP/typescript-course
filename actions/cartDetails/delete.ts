'use server';

import { actionResult } from '@/types';
import { axiosInstance } from '@/utils/axios';
import { revalidatePath } from 'next/cache';

export type TDeleteNoteDefaultValue = {
  id: number;
};

export async function deleteCartDetails(id: number): Promise<actionResult> {
  try {
    await axiosInstance.delete(`/cartDetail/${id}`);
    revalidatePath('/cartDetail');
    return 'success';
  } catch (error) {
    return 'failed';
  }
}
