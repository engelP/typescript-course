'use server';

import { axiosResponse } from '@/types/';
import { axiosInstance } from '@/utils/axios';
import { revalidatePath } from 'next/cache';

export async function getCartUser(): Promise<number> {
  try {
    const {
      data: { body },
    } = await axiosInstance.get<axiosResponse<number>>(`/carts`);

    revalidatePath('/carts');

    return body.data;
  } catch (error) {
    console.error(error);
  }

  return 0;
}
